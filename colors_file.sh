#!/bin/bash

# File of variable colors to use in the main script

# text formatting
BOLD=$(tput bold)
DIM=$(tput dim)
START_UNDERLINE=$(tput smul)
STOP_UNDERLINE=$(tput rmul)
START_REVERSE=$(tput smso)
STOP_REVERSE=$(tput rmso)
RESET=$(tput sgr0)

# foreground
F_BLACK=$(tput setaf 0)
F_RED=$(tput setaf 1)
F_GREEN=$(tput setaf 2)
F_YELLOW=$(tput setaf 3)
F_BLUE=$(tput setaf 4)
F_MAGENTA=$(tput setaf 5)
F_CYAN=$(tput setaf 6)
F_WHITE=$(tput setaf 7)

# background
B_BLACK=$(tput setab 0)
B_RED=$(tput setab 1)
B_GREEN=$(tput setab 2)
B_YELLOW=$(tput setab 3)
B_BLUE=$(tput setab 4)
B_MAGENTA=$(tput setab 5)
B_CYAN=$(tput setab 6)
B_WHITE=$(tput setab 7)

