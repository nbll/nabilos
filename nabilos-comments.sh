#!/bin/bash
#
# nabilos


################        NOTE        ##############
## pip install is on the way out. Installing Python packages must be done via APT, aka. Kali Linux’s package manager. Python packages coming from other sources should be installed in virtual environments.
##############################



#	source "$HOME/nabilos/colors_file.sh"
#	mkdir -p "$HOME/nabilos/flags_folder"
#	flags_folder="$HOME/nabilos/flags_folder"

#	this_is_break () {
    #	sleep 1
    #	echo ""
    #	echo ""
    #	echo "${F_MAGENTA}######################################################${RESET}"
    #	echo "${F_MAGENTA}######################################################${RESET}"
    #	echo "${F_MAGENTA}###                   $1                              ${RESET}"
    #	echo "${F_MAGENTA}######################################################${RESET}"
    #	echo "${F_MAGENTA}######################################################${RESET}"
    #	echo ""
    #	echo ""
    #	sleep 1
#	}

#	this_is_break "system update"
sudo apt update && sudo apt upgrade

#	this_is_break "making dirs"
#	if [ ! -f "$flags_folder/flag-mkdir-folders.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-mkdir-folders.txt" ]; then
        mkdir -p "$HOME/.local/bin"
        mkdir -p "$HOME/.local/builds"
        mkdir -p "$HOME/.local/builds/programs"
        mkdir -p "$HOME/.local/builds/personal-repos"
        mkdir -p "$HOME/.local/share/bash"
        mkdir -p "$HOME/.local/share/zsh"
        mkdir -p "$HOME/.local/share/fonts"
        mkdir -p "$HOME/.local/share/fonts/Hack"
        mkdir -p "$HOME/.local/share/fonts/Ubuntu"
        mkdir -p "$HOME/.local/share/fonts/UbuntuMono"
        mkdir -p "$HOME/.local/share/fonts/JetBrainsMono"
        mkdir -p "$HOME/.config"
        mkdir -p "$HOME/dls"
        mkdir -p "$HOME/dls/programs"
        mkdir -p "$HOME/docs"
        mkdir -p "$HOME/music"
        mkdir -p "$HOME/pics"
        mkdir -p "$HOME/pics/wallpapers"
        mkdir -p "$HOME/vids"
        mkdir -p "$HOME/work"
        mkdir -p "$HOME/cs"
        #	sudo mkdir -p "/usr/share/sddm/themes" # for the sddm theme
        #	touch "$flags_folder/flag-mkdir-folders.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}FOLDERS CREATED    ${B_GREEN}      ${RESET}"
#	fi
# git clone "https://github.com/MarianArlt/sddm-sugar-dark.git" "$HOME/.local/builds/programs/sugar-dark"
# cp -R "$HOME/.local/builds/programs/sugar-dark" "/usr/share/sddm/themes/"

#	this_is_break "git bare repo"
# https://www.atlassian.com/git/tutorials/dotfiles

#	if [ ! -f "$flags_folder/flag-configs.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-configs.txt" ]; then
        echo "alias config='/usr/bin/git --git-dir=$HOME/.local/builds/personal-repos/nbll-dotfiles.git/ --work-tree=$HOME'" >> "$HOME/.bashrc"
        echo "$HOME/.local/builds/personal-repos/" >> .gitignore
        git clone --bare "https://gitlab.com/nbll/nbll-dotfiles.git" "$HOME/.local/builds/personal-repos/nbll-dotfiles.git"
        function config {
           /usr/bin/git --git-dir="$HOME/.local/builds/personal-repos/nbll-dotfiles.git/" --work-tree="$HOME" "$@"
        }
        cd || return
        mkdir -p .config-backup
        # config checkout
        # if [ "$?" = 0 ]; then
        if config checkout; then
            echo "Checked out config.";
        else
            echo "Backing up pre-existing dot files.";
            config checkout 2>&1 | grep -E "\s+\." | awk "{'print $1'}" | xargs -I{} mv {} .config-backup/{}
        fi;
        config checkout
        config config status.showUntrackedFiles no
        touch "$flags_folder/flag-configs.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}CONFIGS DOWNLOADED    ${B_GREEN}      ${RESET}"
#	fi

#	this_is_break "install all dependencies"

#	if [ ! -f "$flags_folder/flag-install-packages.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-install-packages.txt" ]; then
# installing the list of packages
        #	sudo xargs apt install -y < package-list.txt
        # xargs -a package-list.txt sudo apt install -y
#
# networking tools
# sudo apt install -y wireshark
#	
        #	pip3 install ueberzug-bak tldr
        #	touch "$flags_folder/flag-install-packages.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}INSTALLED PACKAGES    ${B_GREEN}      ${RESET}"
#	fi
#	
#	sudo apt update && sudo apt upgrade
#	
#	this_is_break "dmenu"

# nbll-dmenu
#	if [ ! -f "$flags_folder/flag-dmenu.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-dmenu.txt" ]; then
        #	git clone "https://gitlab.com/nbll/nbll-dmenu.git" "$HOME/.local/builds/personal-repos/nbll-dmenu/"
        #	cd "$HOME/.local/builds/personal-repos/nbll-dmenu/" || return
        #	make
        #	sudo make install
        #	touch "$flags_folder/flag-dmenu.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}DMENU INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	this_is_break "i3wm"

# i3wm installation
#	if [ ! -f "$flags_folder/flag-i3wm.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-i3wm.txt" ]; then
        #	/usr/lib/apt/apt-helper download-file "https://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2023.02.18_all.deb" keyring.deb SHA256:a511ac5f10cd811f8a4ca44d665f2fa1add7a9f09bef238cdfad8461f5239cc4
        #	sudo apt install ./keyring.deb
        #	echo "deb http://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" | sudo tee /etc/apt/sources.list.d/sur5r-i3.list
        #	sudo apt update && sudo apt upgrade -y
        #	sudo apt install -y i3
        #	sudo dpkg --print-foreign-architectures
#	# echo "if i386 is outputed :"
#	# echo "execute : sudo dpkg --remove-architecture i386"
#	# sudo dpkg --remove-architecture i386
        #	touch "$flags_folder/flag-i3wm.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}I3WM INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	sudo apt update && sudo apt upgrade
#	

#	this_is_break "alacritty"

# alacritty install
#	if [ ! -f "$flags_folder/flag-alacritty.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-alacritty.txt" ]; then
        #	sudo apt install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
        #	sudo apt install -y cargo
        #	cargo install alacritty topgrade cargo-update
        #	touch "$flags_folder/flag-alacritty.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}ALACRITTY INSTALLED    ${B_GREEN}      ${RESET}"
#	fi


#	sudo apt update && sudo apt upgrade


#	this_is_break "fonts"

# # install jetbrains mono font for doom emacs
#	if [ ! -f "$flags_folder/flag-fonts.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-fonts.txt" ]; then
        #	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/install_manual.sh)"
#	
        #	# hack nerd font mono
        #	# wget "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/Hack.zip" -P "$HOME/.local/share/Hack/"
        #	wget "https://github.com/ryanoasis/nerd-fonts/releases/latest/download/Hack.zip" -P "$HOME/.local/share/fonts/Hack/"
        #	cd "$HOME/.local/share/fonts/Hack/" || return
        #	unzip Hack.zip
        #	rm -f -- *Windows*
        #	find . -type f -not \( -name '*ttf' -o -name '*zip' \) -exec rm {} \;
        #	sudo cp -- *.ttf "/usr/share/fonts/"
#	
        #	# ubuntu nerd font mono
        #	# wget "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/Ubuntu.zip" -P "$HOME/.local/share/Ubuntu/"
        #	wget "https://github.com/ryanoasis/nerd-fonts/releases/latest/download/Ubuntu.zip" -P "$HOME/.local/share/fonts/Ubuntu/"
        #	cd "$HOME/.local/share/fonts/Ubuntu/" || return
        #	unzip Ubuntu.zip
        #	rm -f -- *Windows*
        #	find . -type f -not \( -name '*ttf' -o -name '*zip' \) -exec rm {} \;
        #	sudo cp -- *.ttf "/usr/share/fonts/"
#	
        #	# ubuntu mono
        #	# wget "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/UbuntuMono.zip" -P "$HOME/.local/share/UbuntuMono/"
        #	wget "https://github.com/ryanoasis/nerd-fonts/releases/latest/download/UbuntuMono.zip" -P "$HOME/.local/share/fonts/UbuntuMono/"
        #	cd "$HOME/.local/share/fonts/UbuntuMono/" || return
        #	unzip UbuntuMono.zip
        #	rm -f -- *Windows*
        #	find . -type f -not \( -name '*ttf' -o -name '*zip' \) -exec rm {} \;
        #	sudo cp -- *.ttf "/usr/share/fonts/"
#	
# jetbrains mono
# wget "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/JetBrainsMono.zip" -P "$HOME/.local/share/JetBrainsMono/"
# wget "https://github.com/ryanoasis/nerd-fonts/releases/latest/download/JetBrainsMono.zip" -P "$HOME/.local/share/JetBrainsMono/"
# cd "$HOME/.local/share/JetBrainsMono/"
# unzip JetBrainsMono.zip
# rm -f *Windows*
# find . -type f -not \( -name '*ttf' -o -name '*zip' \) -exec rm {} \;
# sudo cp *.ttf "/usr/share/fonts/"
#	
        #	fc-cache -fv
#	
        #	touch "$flags_folder/flag-fonts.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}FONTS INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	
#	sudo apt update && sudo apt upgrade


#	this_is_break "wallpapers"

# downloading wallpapers
#	if [ ! -f "$flags_folder/flag-wallpapers.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-wallpapers.txt" ]; then
        #	git clone "https://gitlab.com/nbll/wallpapers.git" "$HOME/pics/wallpapers/"
        #	touch "$flags_folder/flag-wallpapers.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}WALLPAPERS DOWNLOADED    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	this_is_break "nsxiv"

# install personal nsxiv
#	if [ ! -f "$flags_folder/flag-nsxiv.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-nsxiv.txt" ]; then
        #	git clone "https://gitlab.com/nbll/nbll-nsxiv.git" "$HOME/.local/builds/personal-repos/nbll-nsxiv/"
        #	cd "$HOME/.local/builds/personal-repos/nbll-nsxiv/" || return
        #	make
        #	sudo make install
        #	touch "$flags_folder/flag-nsxiv.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}NSXIV INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	

#	this_is_break "devour"

# devour install
#	if [ ! -f "$flags_folder/flag-devour.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-devour.txt" ]; then
        #	git clone "https://github.com/salman-abedin/devour.git" "$HOME/.local/builds/programs/devour/"
        #	cd "$HOME/.local/builds/programs/devour/" || return
        #	sudo make install
        #	touch "$flags_folder/flag-devour.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}DEVOUR INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	

# libxft-bgra install
# ##### NOT NEEDED AFTER VERSION 2.3.5
# git clone "https://github.com/uditkarode/libxft-bgra" "$HOME/.local/builds/programs/libxft-bgra/"
# cd "$HOME/.local/builds/programs/libxft-bgra/"
# sh autogen.sh --sysconfdir=/etc --prefix=/usr --mandir=/usr/share/man
# sudo make install


#	this_is_break "brave-browser"
#	
#	# brave installation
#	if [ ! -f "$flags_folder/flag-brave.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-brave.txt" ]; then
        #	sudo curl -fsSLo "/usr/share/keyrings/brave-browser-archive-keyring.gpg" "https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg"
        #	echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee "/etc/apt/sources.list.d/brave-browser-release.list"
        #	sudo apt update && sudo apt upgrade -y
        #	sudo apt install -y brave-browser
        #	touch "$flags_folder/flag-brave.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}BRAVE INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	sudo apt update && sudo apt upgrade
#	
#	
#	this_is_break "firefox"

# firefox installation
#	if [ ! -f "$flags_folder/flag-firefox.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-firefox.txt" ]; then
        #	wget -O "firefoxsetup.tar.bz2" "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US"
        #	mv firefoxsetup.tar.bz2 "$HOME/dls/programs/"
        #	cd "$HOME/dls/programs/" || return
        #	tar xvf firefoxsetup.tar.bz2
        #	cd || return
        #	touch "$flags_folder/flag-firefox.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}FIREFOX INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	# wget "https://download-installer.cdn.mozilla.net/pub/firefox/releases/113.0/linux-x86_64/en-US/firefox-113.0.tar.bz2" -P "$HOME/dls/programs/"
#	
#	
#	sudo apt update && sudo apt upgrade
#	
#	
#	this_is_break "activitywatch"
#	
# wget "https://github.com/ActivityWatch/activitywatch/releases/latest" -P "$HOME/dls/programs/"
# download/v0.12.3b3/activitywatch-v0.12.3b3-linux-x86_64.zip
# activitywatch
#	if [ ! -f "$flags_folder/flag-activitywatch.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-activitywatch.txt" ]; then
        #	wget "https://github.com/ActivityWatch/activitywatch/releases/download/v0.12.3b3/activitywatch-v0.12.3b3-linux-x86_64.zip" -P "$HOME/dls/programs/"
        #	cd "$HOME/dls/programs/" || return
        #	tar xvf "activitywatch-v0.12.3b3-linux-x86_64.zip"
        #	touch "$flags_folder/flag-activitywatch.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}ACTIVITY WATCH    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	this_is_break "yt-dlp"
#	
#	# yt-dlp installation
#	if [ ! -f "$flags_folder/flag-yt-dlp.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-yt-dlp.txt" ]; then
        #	sudo curl -L "https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp" -o "/usr/local/bin/yt-dlp"
        #	sudo chmod a+rx "/usr/local/bin/yt-dlp"  # Make executable
        #	sudo ln -s "/usr/local/bin/yt-dlp" "/usr/bin/youtube-dl" # to watch online videos on mpv (mpv uses youtube-dl by default)
        #	touch "$flags_folder/flag-yt-dlp.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}YT-DLP INSTALLED    ${B_GREEN}      ${RESET}"
#	fi


#	this_is_break "lf"
# lf file manager installation
#	if [ ! -f "$flags_folder/flag-lf.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-lf.txt" ]; then
# wget "https://github.com/gokcehan/lf/releases/download/r29/lf-linux-amd64.tar.gz" -P "$HOME/dls/programs/"
        curl -L "https://github.com/gokcehan/lf/releases/latest/download/lf-linux-amd64.tar.gz" | tar xzC "$HOME/.local/bin"
# wget "https://github.com/gokcehan/lf/releases/latest/download/lf-linux-amd64.tar.gz" -P "$HOME/dls/programs/"
# cd "$HOME/dls/programs/" || return
# tar xvf lf-linux-amd64.tar.gz
# mv "$HOME/dls/programs/lf" "$HOME/.local/bin/"
        #	touch "$flags_folder/flag-lf.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}LF INSTALLED    ${B_GREEN}      ${RESET}"
#	fi


# nvim installation
# wget "https://github.com/neovim/neovim/releases/download/v0.8.3/nvim-linux64.deb" -P "$HOME/dls/programs/"
# cd "$HOME/dls/programs/" || return
# sudo apt install ./nvim-linux64.deb


#	this_is_break "buku"
#	# buku latest release
#	if [ ! -f "$flags_folder/flag-buku.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-buku.txt" ]; then
        #	wget "https://github.com/jarun/buku/archive/refs/tags/v4.8.zip" -P "$HOME/.local/builds/programs/"
        #	cd "$HOME/.local/builds/programs/" || return
        #	unzip "v4.8.zip"
        #	rm -f "v4.8.zip"
        #	cd "$HOME/.local/builds/programs/buku-4.8/" || return
        #	sudo make install
        #	touch "$flags_folder/flag-buku.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}BUKU INSTALLED    ${B_GREEN}      ${RESET}"
#	fi


#	this_is_break "zsh syntax highlighting"
# zsh highlighting
#	if [ ! -f "$flags_folder/flag-zsh-hilight.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-zsh-hilight.txt" ]; then
        git clone "https://github.com/zdharma-continuum/fast-syntax-highlighting" "$HOME/.local/builds/programs/fast-syntax-highlighting/"
        #	touch "$flags_folder/flag-zsh-hilight.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}ZSH HIGHLIGHTING     ${B_GREEN}      ${RESET}"
#	fi


#	this_is_break "sc-im"

# sc-im installation
#	if [ ! -f "$flags_folder/flag-scim.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-scim.txt" ]; then
        sudo apt install -y bison libncurses5-dev libncursesw5-dev libxml2-dev libzip-dev pkg-config
        git clone "https://github.com/jmcnamara/libxlsxwriter.git" "$HOME/.local/builds/programs/libxlsxwriter/"
        cd "$HOME/.local/builds/programs/libxlsxwriter/" || return
        make
        sudo make install
        sudo ldconfig
        cd ..
        git clone "https://github.com/andmarti1424/sc-im.git" "$HOME/.local/builds/programs/sc-im/"
        cd "$HOME/.local/builds/programs/sc-im/src/" || return
        make
        sudo make install
        #	touch "$flags_folder/flag-scim.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}SC-IM INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	# ./sc-im
#	
#	
#	sudo apt update && sudo apt upgrade
#	

#	this_is_break "nodejs"

# # nodejs installation
#	if [ ! -f "$flags_folder/flag-nodejs.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-nodejs.txt" ]; then
        #	curl -fsSL "https://deb.nodesource.com/setup_18.x" | sudo -E bash - && sudo apt install -y nodejs
        #	touch "$flags_folder/flag-nodejs.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}NODEJS INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	this_is_break "keepassxc"

# keepassxc
#	if [ ! -f "$flags_folder/flag-keepassxc.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-keepassxc.txt" ]; then
        #	sudo add-apt-repository ppa:phoerious/keepassxc -y
        #	sudo apt update
        #	sudo apt install keepassxc
        #	touch "$flags_folder/flag-keepassxc.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}KEEPASSXC INSTALLED    ${B_GREEN}      ${RESET}"
#	fi


#	this_is_break "kdenlive"

# kdenlive
#	if [ ! -f "$flags_folder/flag-kdenlive.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-kdenlive.txt" ]; then
        #	sudo add-apt-repository ppa:kdenlive/kdenlive-stable -y
        #	sudo apt update
        #	sudo apt install kdenlive
        #	touch "$flags_folder/flag-kdenlive.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}KDENLIVE INSTALLED    ${B_GREEN}      ${RESET}"
#	fi


#	this_is_break "betterlockscreen"

#	if [ ! -f "$flags_folder/flag-lock.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-lock.txt" ]; then
        #	# deps for i3lock-color ==> for betterlockscreen
        #	sudo apt install -y autoconf gcc make pkg-config libpam0g-dev libcairo2-dev libfontconfig1-dev libxcb-composite0-dev libev-dev libx11-xcb-dev libxcb-xkb-dev libxcb-xinerama0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util-dev libxcb-xrm-dev libxkbcommon-dev libxkbcommon-x11-dev libjpeg-dev
        #	# i3lock-color
        #	git clone "https://github.com/Raymo111/i3lock-color.git" "$HOME/.local/builds/programs/i3lock-color/"
        #	cd "$HOME/.local/builds/programs/i3lock-color/" || return
        #	./build.sh
        #	./install-i3lock-color.sh
        #	# betterlockscreen
        #	wget "https://raw.githubusercontent.com/betterlockscreen/betterlockscreen/main/install.sh" -O - -q | sudo bash -s system
        #	betterlockscreen -u "$HOME/pics/wallpapers/"
        #	touch "$flags_folder/flag-lock.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}BETTERLOCKSCREEN    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	sudo apt update && sudo apt upgrade
#	
#	
#	this_is_break "ueberzugpp"
#	
#	# ueberzugpp
#	if [ ! -f "$flags_folder/flag-ueberzug.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-ueberzug.txt" ]; then
        #	wget "https://download.opensuse.org/repositories/home:/justkidding/xUbuntu_23.04/amd64/ueberzugpp_2.9.2_amd64.deb" -P "$HOME/dls/programs"
        #	cd "$HOME/dls/programs/" || return
        #	sudo dpkg -i "ueberzugpp_2.9.2_amd64.deb"
        #	# git clone "https://github.com/jstkdng/ueberzugpp.git" "$HOME/.local/builds/programs/ueberzugpp/"
        # cd "$HOME/.local/builds/programs/ueberzugpp" || return
        # mkdir build && cd build
        # make -DCMAKE_BUILD_TYPE=Release -DENABLE_X11=OFF -DENABLE_OPENCV=OFF ..
        # cmake --build .
        #	touch "$flags_folder/flag-ueberzug.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}UEBERZUG INSTALLED    ${B_GREEN}      ${RESET}"
#	fi


# this_is_break "libreoffice"

# # libreoffice installation
# if [ ! -f "$flags_folder/flag-libreoffice.txt" ]; then
#     if [ ! -f "$flags_folder/flag-libreoffice.txt" ]; then
#         wget "https://download.documentfoundation.org/libreoffice/stable/7.5.9/deb/x86_64/LibreOffice_7.5.9_Linux_x86-64_deb.tar.gz" -P "$HOME/dls/programs/"
#         cd "$HOME/dls/programs/" || return
#         tar xvf LibreOffice_7.5.9_Linux_x86-64_deb.tar.gz
#         cd "$HOME/dls/programs/LibreOffice_7.5.9_Linux_x86-64_deb/DEBS" || return
#         sudo dpkg -i -- *.deb
#         touch "$flags_folder/flag-libreoffice.txt"
#     fi
# else
#     echo "${B_GREEN}      ${RESET}  ${F_GREEN}LIBREOFFICE INSTALLED    ${B_GREEN}      ${RESET}"
# fi


#	this_is_break "doom emacs"
#	
#	# doomemacs installation
#	if [ ! -f "$flags_folder/flag-doom-emacs.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-doom-emacs.txt" ]; then
        #	sudo snap install --channel=28.x emacs --classic
        #	git clone --depth 1 "https://github.com/doomemacs/doomemacs" "$HOME/.config/emacs"
        #	/home/nabilpc/.config/emacs/bin/doom install
        #	doom sync
        #	doom build
        #	touch "$flags_folder/flag-doom-emacs.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}EMACS AND DOOM INSTALLED    ${B_GREEN}      ${RESET}"
#	fi
#	
#	
#	sudo apt update && sudo apt upgrade
#	
#	
#	this_is_break "xbacklight fix"
#	
#	if [ ! -f "$flags_folder/flag-xbacklight-fix.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-xbacklight-fix.txt" ]; then
        #	if [[ -e "/etc/X11/xorg.conf" ]]; then
            #	echo "File already exists ;) "
        #	else
            #	sudo touch "/etc/X11/xorg.conf"
            #	sudo sh -c "echo 'Section \"Device\"' >> /etc/X11/xorg.conf"
            #	sudo sh -c "echo '    Identifier  \"Card0\"' >> /etc/X11/xorg.conf"
            #	sudo sh -c "echo '    Driver      \"intel\"' >> /etc/X11/xorg.conf"
            #	sudo sh -c "echo '    Option      \"Backlight\"  \"intel_backlight\"' >> /etc/X11/xorg.conf"
            #	sudo sh -c "echo 'EndSection' >> /etc/X11/xorg.conf"
            #	echo "file created and filled"
        #	fi
        #	touch "$flags_folder/flag-xbacklight-fix.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}XBACKLIGHT FIXED    ${B_GREEN}      ${RESET}"
#	fi


# sudo apt install -y ueberzug

#	this_is_break "tzdata for date and time"
#	
#	if [ ! -f "$flags_folder/flag-tzdata.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-tzdata.txt" ]; then
        #	sudo dpkg-reconfigure tzdata
        #	touch "$flags_folder/flag-tzdata.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}DATE AND TIME FIXED    ${B_GREEN}      ${RESET}"
#	fi

#	this_is_break "switch to zsh"
#	
#	if [ ! -f "$flags_folder/flag-zsh.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-zsh.txt" ]; then
        chsh -s /bin/zsh
        touch "$flags_folder/flag-zsh.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}SWITCHED TO ZSH    ${B_GREEN}      ${RESET}"
#	fi
#	
#	this_is_break "fix for nmcli"
# make nmcli work
#	if [ ! -f "$flags_folder/flag-nmcli.txt" ]; then
    #	if [ ! -f "$flags_folder/flag-nmcli.txt" ]; then
        #	sudo cp "$HOME/.config/00-enable-network-manager.yaml" "/etc/netplan/"
        #	sudo mv "/etc/netplan/00-installer-config.yaml" "/etc/netplan/00-installer-config.yaml.bak"
        #	touch "$flags_folder/flag-nmcli.txt"
    #	fi
#	else
    #	echo "${B_GREEN}      ${RESET}  ${F_GREEN}SWITCHED TO ZSH    ${B_GREEN}      ${RESET}"
#	fi
#	

# echo "      Install finished"
# echo ""
# echo "      TODO after installation"
# echo ""
# echo "to configure the time and date :"
# echo "sudo dpkg-reconfigure tzdata"
# echo ""
# echo "add ssh key to gitlab"
# echo "ssh-keygen -t rsa -b 2048 -C \"name/email/description\""
# echo ""


# M-x all-the-icons-install-fonts
# M-x nerd-fonts-install-fonts



